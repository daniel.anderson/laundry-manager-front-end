# Laundry Manager by Kelompok Marjinal

Front-end dari proyek ini dapat dilihat pada [https://laundry-manager.netlify.app/](https://laundry-manager.netlify.app/)

Laundry Manager terinspirasi dari aplikasi money manager yang cukup banyak digunakan oleh orang orang.

Aplikasi web laundry manager dapat digunakan untuk sisi pengguna jasa laundry. Seperti money manager yang mengelola pemasukan dan pengeluaran uang, laundry manager ini mengelola "pengeluaran" dan "pengambilan" pakaian ke/dari laundry.

Halaman gitlab ini merupakan repositori dari bagian front-end laundry manager. Proses pengembangan dilakukan menggunakan React.js

## Fitur

Laundry manager memiliki beberapa fitur, di antaranya mengelola pakaian yang di-laundry dan statistik.

### Statistik
Halaman statistik merupakan halaman yang menampilkan statistik dari "transaksi" laundry yang dilakukan oleh pengguna. Jangka waktu yang dipilih ada 3, yaitu 1 minggu, 1 bulan, dan 3 bulan.

Untuk sumber kode, halaman statistik terdiri dari 3 file, yaitu Statistic.js, ChartCard.js, dan DetailCard.js. 

Statistic.js merupakan komponen utama dari halaman statistik di mana terdapat komponen untuk memilih jangka waktu memanfaatkan UI Framework [material-ui](https://material-ui.com/) bernama FormControl.

DetailCard.js merupakan komponen pendukung Statistic.js yang menampilkan rata-rata laundry, jumlah transaksi laundry, dan uang yang dikeluarkan untuk laundry. Untuk DetailCard.js, terdapat toggle untuk memilih antara laundry kilo atau satuan. Dari pilihan toggle dan jangka waktu, akan dilakukan fetch ke API di backend.
Toggle laundry kilo dan satuan menggunakan komponen ToggleButton

ChartCard.js merupakan komponen lainnya pada halaman statistik yang menampilkan Pie Chart dari jenis dan jumlah pakaian satuan yang dicuci. Penampilan dari Pie Chart menggunakan Framework [DevExtreme Reactive](https://devexpress.github.io/devextreme-reactive/react/chart/demos/pie/pie/).


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
