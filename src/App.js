import React from "react";
import './App.css';
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Statistic from "./components/Statistic";
import TransactionForm from "./components/TransactionForm"
import Dummy from "./components/dummy";
import TransactionList from "./components/TransactionList";


function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <div className="content">
          <Switch>
            <Route path="/transaction" component={() => <TransactionList/>} />
            <Route path="/statistic" component={() => <Statistic />} />
            <Route path="/createTransaction" component={() => <TransactionForm />} />
            <Route path="/updateTransaction" component={() => <Dummy />} />
          </Switch>
        </div>
      </Router>

    </div>
  );
}

export default App;
