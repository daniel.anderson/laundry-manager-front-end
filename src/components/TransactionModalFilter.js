import './TransactionList.css';
import * as TransactionListConts from './TransactionListStyle';
import TransactionFormControl from './TransactionFormControl';
import CurrencyInput from 'react-currency-input';

const Button = TransactionListConts.ButtonModal;
const ModalTitle = TransactionListConts.ModalTitle;
const ModalLabel = TransactionListConts.ModalLabel;
const ModalBody = TransactionListConts.ModalBody;
const ColoredLine = TransactionListConts.ModalColorLine;

const initialValues = {
  laundryTypeName: "",
  kiloOrUnit: "Semua",
  checkInStartDate: "",
  checkOutStartDate: "", 
  minPrice: 0,
  maxPrice: 0,
};

const Modal = ({ handleClose, show, filterHandle, children }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";
  const {
    values,
    handleChange,
    handleCurrenyChange,
    handleSubmit
  } = TransactionFormControl({
    initialValues,
    onSubmit: values => {filterHandle(values)}
  });
  
  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        <form onSubmit={handleSubmit}>
          <ModalTitle>Saring Tabel</ModalTitle>

          <ModalBody>
            <ModalLabel>Jenis Pakaian</ModalLabel>
            <input
              type="text"
              name="laundryTypeName"
              onChange={handleChange}
              value={values.laundryTypeName}
            />
            <br />

            <ModalLabel>Tipe Laundry</ModalLabel>
            <select 
              value={values.kiloOrUnit}
              onChange={handleChange}
              name='kiloOrUnit'>
                <option name="semua">Semua</option>
                <option name="kiloan">Kiloan</option>
                <option name="satuan">Satuan</option>
            </select>
            <br/>

            <ModalLabel>Tanggal Masuk</ModalLabel>
            <input
              type="date"
              name="checkInStartDate"
              onChange={handleChange}
              value={values.checkInStartDate}
            />
            <br />

            <ModalLabel>Tanggal Keluar</ModalLabel>
            <input
              type="date"
              name="checkOutStartDate"
              onChange={handleChange}
              value={values.checkOutStartDate}
            />
            <br />
            <div className="inline">
            <ModalLabel>Harga Satuan</ModalLabel>
            <CurrencyInput
              name="minPrice"
              onChangeEvent={handleCurrenyChange}
              value={values.minPrice}
              thousandSeparator="."
              prefix="Rp."
              precision="0"
            />
            <ColoredLine/>
            <CurrencyInput
              name="maxPrice"
              onChangeEvent={handleCurrenyChange}
              value={values.maxPrice}
              thousandSeparator="."
              prefix="Rp."
              precision="0"
            />
            </div>
            <br />
          </ModalBody>
          <Button type="button" onClick={handleClose}>Kembali</Button>
          <Button type="submit">Submit</Button>
        </form>
        
      </section>
    </div>
  );
};

export default Modal;