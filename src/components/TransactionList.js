import React, { useState, useEffect, useMemo } from 'react';
import axios from 'axios';
import Modal from './TransactionModalFilter.js';
import 'dateformat';

import * as TransactionListConts from './TransactionListStyle';

import Link from '@material-ui/core/Link';

import filterImage from '../assets/filter.png';

import Table from './TransactionTable';
import '../App.css';
import './TransactionList.css';

const TableStyle = TransactionListConts.Table;
const TableBackground = TransactionListConts.backgorundTable;
const Button = TransactionListConts.Button;

function isEmpty(ob){
  for(var i in ob){ return false;}
 return true;
}

function numberFormat(num) {
  return 'Rp.' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

var dateFormat = require("dateformat");
dateFormat.i18n = {
  dayNames: [
    "Sun",
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu",
  ],
  monthNames: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ],
};

function TransactionList() {
  const columns = useMemo(
    () => 
    [
      {Header: 'Tanggal Masuk',
        id: 'tanggalMasuk',
        accessor: data => dateFormat(data.checkInDate, "yyyy, d mmmm - dddd"),
      },
      {Header: 'Tanggal Selesai',
      accessor: data => dateFormat(data.checkOutDate, "yyyy, d mmmm - dddd"),
      },
      {Header: 'Jenis',
        accessor: 'laundryTypeName',
      },
      {Header: 'Jumlah',
        accessor: data => isNaN(data.numPieces) ? data.weight + " kg": isNaN(data.weight) ? data.numPieces + " unit": 'null'
      },
      {Header: 'Harga Satuan',
        accessor: data => isNaN(data.pricePerPiece) ? numberFormat(data.pricePerWeight) : isNaN(data.pricePerWeight) ? numberFormat(data.pricePerPiece): 'null'
      },
      {Header: 'Total Harga',
        accessor: data => isNaN(data.pricePerPiece) ? numberFormat((data.pricePerWeight)*(data.weight)) : isNaN(data.pricePerWeight) ? numberFormat((data.pricePerPiece)*(data.numPieces)): 'null'
      },
      {Header: 'Deskripsi Transaksi',
        accessor: 'description',
      },
    ],
    []
  );

  const [isLoading, setLoading] = useState(true)
  const [isError, setError] = useState(false)
  const [data, setData] = useState([]);

  const [show, setShow] = useState(false);

  const [filter, setFilter] = useState({
    laundryTypeName: "",
    kiloOrUnit: "",
    checkInStartDate: "",
    checkOutStartDate: "", 
    minPrice: 0,
    maxPrice: 0,
  });

  const [kilogramLink, setKilogramLink] = useState('kilogram')
  const [unitLink, setUnitLink] = useState('unit')
  const [laundryType, setLaundryType] = useState('')

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if(isEmpty(data)){
      setLoading(true)
    } else {
      setLoading(false)
    }
  }, [data]);

  useEffect(() => {
    fetchData()
  }, []);

  useEffect(() => {
    fetchData()
  }, [unitLink])

  useEffect(() => {
    let appendLink = ""
    let kiloLink = ""
    let unitLink = ""
    let kiloPrice = ["", ""]
    let unitPrice = ["", ""]

    if (filter.laundryTypeName != ""){
      appendLink += "&laundryTypeName=" + filter.laundryTypeName
    }
    else if(filter.checkInStartDate != ""){
      appendLink += "&checkInStartDate=" + filter.checkInStartDate + "T00:00:00Z"
    }
    else if(filter.checkOutStartDate != ""){
      appendLink += "&checkOutStartDate=" + filter.checkOutStartDate + "T22:59:24Z"
    }
    else if (filter.minPrice > 0){
      kiloPrice[0] = "minPricePerWeight=" + filter.minPrice
      unitPrice[0] = "minPricePerPiece=" + filter.minPrice
    }
    else if (filter.maxPrice > 0){
      kiloPrice[1] = "maxPricePerWeight=" + filter.maxPrice
      unitPrice[1] = "maxPricePerPiece=" + filter.maxPrice
    } else {
      setKilogramLink("kilogram")
      setUnitLink("unit")
      fetchData()
    }
    

    if(kiloPrice[0] == ""){
      if(kiloPrice[1] != ""){
        kiloLink = "kilogram" + "?" + kiloPrice[1]
        unitLink = "unit" + "?" + unitPrice[1]
      } else {
        kiloLink = "kilogram?minPricePerWeight=0"
        unitLink = "unit?minPricePerPiece=0"
      }
    } else {
      if(kiloPrice[1] == ""){
        kiloLink = "kilogram" + "?" + kiloPrice[0]
        unitLink = "unit" + "?" + unitPrice[0]
      } else {
        kiloLink = "kilogram" + "?" + kiloPrice[0] + "&" + kiloPrice[1]
        unitLink = "unit" + "?" + unitPrice[0] + "&" + unitPrice[1]
      }
    }

    setKilogramLink(kiloLink+appendLink)
    setUnitLink(unitLink+appendLink)
  }, [filter])

  const fetchData = async () => {
    setError(false);
    setLoading(true);

    try {
      const dafaultLink = "https://marjinal-laundry.herokuapp.com/api/laundry/"

      if(laundryType == "" || laundryType == "Semua"){
        const responseKilogram = await axios(dafaultLink + kilogramLink);
        const responseUnit = await axios(dafaultLink + unitLink);
        
        const valueUnit = responseUnit.data.data;
        const valueKilogram = responseKilogram.data.data;

        setData([...valueUnit, ...valueKilogram]);
      }
      else if (laundryType == "Kiloan") {
        console.log('masuk kilo')
        const responseKilogram = await axios(dafaultLink + kilogramLink);
        const valueKilogram = responseKilogram.data.data;
        setData(valueKilogram);
      } else {
        console.log('masuk satuan')
        const responseUnit = await axios(dafaultLink + unitLink);
        const valueUnit = responseUnit.data.data;

        setData(valueUnit);
      }
    } catch (error) {
      setError(true);
    }
  };

  const filterHandle = (values) => {
    setLaundryType(values.values.kiloOrUnit)
    setFilter(values.values)
    handleClose();
  }

  return ( 
    <div className="App">
      <div>
      <Link href="/createTransaction">
        <Button> 
          Tambah Transaksi
        </Button>
      </Link>
      <Button onClick={handleShow} >
        <img src={filterImage} style={{"margin-right":"0.5rem"}}/>
        Saring
      </Button>
      </div>
      
      <TableBackground>
        <TableStyle>
        {isLoading ? (
          <div>Loading ...</div>
          ) : 
          isError ? (
            <div>Error ...</div>
          ) :
            (
          <Table 
          columns={columns} 
          data={data}/>
          )}
        </TableStyle>
      </TableBackground>

      <Modal show={show} handleClose={handleClose} filterHandle={filterHandle}>
          <p>Modal</p>
        </Modal>
    </div>
  )
}


export default TransactionList;