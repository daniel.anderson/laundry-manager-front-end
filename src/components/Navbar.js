import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    color: "#1c3653",
  },
}));

export default function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ backgroundColor: '#ffffff' }} >
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="primary" aria-label="menu">
              Logo
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Marjinal Laundry Manager
          </Typography>
          <Link href="/transaction" color="inherit">
            <Button color="#1c3653">
              Daftar Transaksi
            </Button>
          </Link>
          <Link href="/statistic" color="inherit">
            <Button color="primary">
              Statistik
            </Button>
          </Link>
          <Link href="#" color="inherit">
            <Button color="primary">
              Keluar
            </Button>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}
