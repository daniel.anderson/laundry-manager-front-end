import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Paper, Container } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Link from '@material-ui/core/Link'
import DatePicker from 'react-datepicker'

import "react-datepicker/dist/react-datepicker.css"

const useStyles = makeStyles((theme) => ({
    form: {
      margin: theme.spacing(30),
      marginTop: theme.spacing(10),
    },
    inputGrid: {
        textAlign: 'left',
        display: 'flex',
        flexDirection: 'column',
    },
    formLabel: {
        margin: theme.spacing(0.5),
        marginLeft: theme.spacing(2),
    },
    inputLarge: {
        borderRadius: theme.spacing(1),
        width: theme.spacing(20),
    },
    inputMedium: {
        borderRadius: theme.spacing(1),
        width: theme.spacing(15),
    },
    inputSmall: {
        borderRadius: theme.spacing(1),
        width: theme.spacing(10),
    },
    totalHargaGrid: {
        textAlign: 'left',
        display: 'flex',
        flexDirection: 'row',
    },
    totalHargaLabel: {
        marginLeft: theme.spacing(2),
    },
    textarea:{
        borderRadius: theme.spacing(1),
        height: theme.spacing(10),
    },
    root2: {
        margin: theme.spacing(4),
    },
    grid: {
        width: '100%',
    },
    batalDiv: {
        backgroundColor: '#BF0505',
        borderRadius: theme.spacing(1),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: theme.spacing(5),
    },
    batalBtn: {
        color: 'white',
        textDecoration: 'none',
    },
    simpanBtn: {
        backgroundColor: '#33C55C',
        color: 'black',
        borderRadius: theme.spacing(1),
        height: theme.spacing(5),
    }
  })
)

function TransactionForm() {
    const classes = useStyles()
    const jenis = [
        {
            label: 'kaos',
            isSatuan: false,
            isKiloan: true,
        },
        {
            label: 'sprei',
            isSatuan: true,
            isKiloan: false,
        },
    ]

    const handleChange = (e) => {
        setSelectedJenis(jenis.find((item) => item.label === e.target.value))
        console.log(selectedJenis)
    }

    const handleSubmit = (e) => {
        console.log({
            'jenis': selectedJenis,
            'unitKiloan': unitKiloan,
            'unitSatuan': unitSatuan,
            'hargaSatuan': hargaSatuan,
            'tanggalMasuk': tanggalMasuk.toString(),
            'tanggalSelesai': tanggalSelesai.toString(),
            'deskripsi': deskripsi,
        })
        e.preventDefault()
    }

    const [selectedJenis, setSelectedJenis] = useState(jenis[0])
    const [unitKiloan, setUnitKiloan] = useState(0)
    const [unitSatuan, setUnitSatuan] = useState(0)
    const [hargaSatuan, setHargaSatuan] = useState(0)
    const [tanggalMasuk, setTanggalMasuk] = useState(new Date())
    const [tanggalSelesai, setTanggalSelesai] = useState(new Date())
    const [deskripsi, setDeskripsi] = useState('-')

    let unitLabel = ''
    let unitInput = ''
    let hargaLabel = ''
    let hargaTotal = 0

    if (selectedJenis.isSatuan) {
        unitLabel = <label className={classes.formLabel}>Jumlah</label>
        unitInput = <input className={classes.inputSmall}
                            onChange={e => setUnitSatuan(parseInt(e.target.value))}
                            type='number' value={unitSatuan} min={0} step={1} required/>
        hargaLabel = <label className={classes.formLabel}>Harga Satuan</label>
        hargaTotal = unitSatuan * hargaSatuan
    } else if (selectedJenis.isKiloan) {
        unitLabel = <label className={classes.formLabel}>Berat</label>
        unitInput = <input className={classes.inputSmall}
            onChange={e => setUnitKiloan(parseFloat(e.target.value))}
            type='text' pattern='([0-9]*[.])?[0-9]+' required/>
        hargaLabel = <label className={classes.formLabel}>Harga Kiloan</label>
        hargaTotal = unitKiloan * hargaSatuan
    }

    return (
        <div>
            <Paper className={classes.root2}>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <Container maxWidth='lg'>
                        <Grid
                            container
                            direction='row'
                            justify='center'
                            alignItems='flex-start'
                            spacing={5}
                            className={classes.grid}
                        >
                            <Grid className={classes.inputGrid} item xs={9} md={5}>
                                <label className={classes.formLabel}>Jenis</label>
                                <select className={classes.inputLarge}
                                    onChange={handleChange}
                                    required
                                >
                                    {jenis.map(item => (
                                        <option
                                            value={item.label}
                                        >
                                            {item.label}
                                        </option>
                                    ))}
                                </select>
                            </Grid>
                            <Grid className={classes.inputGrid} item xs={9} md={4}>
                                <label className={classes.formLabel}>Tanggal Masuk</label>
                                <DatePicker className={classes.inputLarge}
                                    required={true}
                                    selected={tanggalMasuk}
                                    onChange={date => setTanggalMasuk(date)}
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            direction='row'
                            justify='center'
                            alignItems='flex-start'
                            spacing={5}
                            className={classes.grid}
                        >
                            <Grid className={classes.inputGrid} item xs={9} md={2}>
                                { unitLabel }
                                { unitInput }
                            </Grid>
                            <Grid className={classes.inputGrid} item xs={9} md={3}>
                                { hargaLabel }
                                <input className={classes.inputMedium}
                                    onChange={(e) => setHargaSatuan(parseInt(e.target.value))}
                                    type='number' value={hargaSatuan} min={0} step={1} required
                                />
                            </Grid>
                            <Grid className={classes.inputGrid} item xs={9} md={4}>
                                <label className={classes.formLabel}>Tanggal Selesai</label>
                                <DatePicker className={classes.inputLarge}
                                    required={true}
                                    selected={tanggalSelesai}
                                    onChange={date => setTanggalSelesai(date)}
                                    minDate={tanggalMasuk}
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            direction='row'
                            justify='center'
                            alignItems='flex-start'
                            spacing={5}
                            className={classes.grid}
                        >
                            <Grid className={classes.totalHargaGrid} item xs={9} md={3}>
                                <label className={classes.totalHargaLabel}>Total Harga</label>
                            </Grid>
                            <Grid className={classes.totalHargaGrid} item xs={9} md={6}>
                                <label>{hargaTotal}</label>
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            direction='row'
                            justify='center'
                            alignItems='flex-start'
                            spacing={5}
                            className={classes.grid}
                        >
                            <Grid className={classes.inputGrid} item xs={9} md={7}>
                                <label className={classes.formLabel}>Deskripsi</label>
                                <textarea className={classes.textarea}
                                    onChange={(e) => setDeskripsi(e.target.value)}/>
                            </Grid>
                            <Grid className={classes.inputGrid} item xs={9} md={2}/>
                        </Grid>
                        <Grid
                            container
                            direction='row'
                            justify='center'
                            alignItems='flex-start'
                            spacing={5}
                            className={classes.grid}
                        >
                            <Grid className={classes.inputGrid} item xs={9} md={3}/>
                            <Grid className={classes.inputGrid} item xs={9} md={3}>
                                <div className={classes.batalDiv}>
                                    <a className={classes.batalBtn} href='/transaction'>Batal</a>
                                </div>
                            </Grid>
                            <Grid className={classes.inputGrid} item xs={9} md={3}>
                                <input className={classes.simpanBtn} type='submit' value='Simpan'/>
                            </Grid>
                        </Grid>
                    </Container>
                </form>
            </Paper>
        </div>
    )
}

export default TransactionForm
