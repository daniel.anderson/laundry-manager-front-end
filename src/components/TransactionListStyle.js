import styled from 'styled-components';

export const Table = styled.div`
  table {
    text-align:center;
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
  .pagination {
    padding: 0.5rem;
  }
`

export const backgorundTable = styled.section`
  background: white;
  margin-left: 5rem;
  margin-right: 5rem;
  margin-top: 1rem;
  padding: 2rem;
  text-align center;
  border-radius: 15px;
  `;

  export const Button = styled.button`
  margin-top:1rem;
  margin-right:1rem;
  background: #FFFFFF;
  border:none;
  border-radius: 10px;
  font-family: Merriweather;
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  line-height: 50px;
  text-align: center;
  align-items: left;
  `;

  export const ButtonModal = styled.button`
  margin:1rem;
  background: #D0EAF1;
  border:none;
  border-radius: 10px;
  font-family: Merriweather;
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  line-height: 50px;
  text-align: center;
  align-items: left;
  `;

  export const ButtonDelete = styled.button`
  margin:0.5rem;
  background: #BF0505;
  border:none;
  border-radius: 10px;
  font-family: Merriweather;
  font-style: normal;
  font-size: 20px;
  line-height: 30px;
  text-align: center;
  align-items: left;
  color: white;
  `;

  export const ButtonUpdate = styled.button`
  margin:0.5rem;
  background: #59a56e;
  border:none;
  border-radius: 10px;
  font-family: Merriweather;
  font-style: normal;
  font-size: 20px;
  line-height: 30px;
  text-align: center;
  align-items: left;
  color:white
  `;

  export const ModalBody = styled.div`
  text-align:left;
  margin-left: 10%;
  `;

  export const ModalTitle = styled.h1`
  background: #D0EAF1;
  margin-block-start: 0;
  line-height: 2.5em;
  `;

  export const ModalLabel = styled.label`
  margin-right: 1rem
  `
  export const ModalInput = styled.input`
  margin-left: 1rem;
  border-color: #D0EAF1;
  `;

  export const ModalColorLine = styled.hr`
  border-block-color: #000000;
  background: #000000;
  color: black;
  height: 2%;
  width: 2%;
  margin-left: 1%;
  margin-right: 1%;
  `