import { useState, useEffect, useRef } from "react";

    const TransactionFormControl = ({
      initialValues,
      onSubmit
    }) => {
      const [values, setValues] = useState(initialValues || {});
      const [errors, setErrors] = useState({});
      const [touched, setTouched] = useState({});

      const formRendered = useRef(true);

      useEffect(() => {
        if (formRendered.current) {
          setValues(initialValues);
          setErrors({});
          setTouched({});
        }
        formRendered.current = false;
      }, [initialValues]);

      const handleChange = (event) => {
        const { target } = event;
        const { name, value } = target;
        event.persist();
        setValues({ ...values, [name]: value });
    };

      const handleCurrenyChange = (event, maskedvalue, floatvalue) => {
        const {target} = event
        const {name} = target;
        setValues({ ...values, [name]: floatvalue });
      };

      const handleSubmit = (event) => {
        if (event){
            event.preventDefault()
        };
        setErrors({ ...errors });
        onSubmit({ values, errors });
      };

      return {
        values,
        errors,
        touched,
        handleChange,
        handleCurrenyChange,
        handleSubmit
      };
    };

    export default TransactionFormControl;
