import React, {useState, useEffect} from 'react';
import {
  Chart,
  PieSeries,
  Title,
  Legend,
} from '@devexpress/dx-react-chart-material-ui';
import axios from 'axios';

const ChartCard = ({dateRange}) => {
  const [url, setUrl] = useState('https://marjinal-laundry.herokuapp.com/api/statistic/unit?filter='+dateRange);
  const [response,setResponse] = useState([]);
  const [chartData,setChartData] = useState([]);
  const [itemType, setItemType] = useState([]);

  useEffect(() => {
    const fetchDetail = async () => {
        try{
            const response = await axios(url);
            const listData = response.data.data;
            setResponse([listData])
        } catch (error) {
            console.log(error);
        }
    };
    fetchDetail();
  }, [url]);

  useEffect(() => {
    if(response[0] !== undefined && response !== []){
      const itemTypes = response[0]
          .map(item => item.laundryTypeName) // get all item types
          .filter((mediaType, index, array) => array.indexOf(mediaType) === index); // filter out duplicates
      const counts = itemTypes
        .map(itemType => ({
          type: itemType,
          count: response[0].filter(item => item.laundryTypeName === itemType).reduce((total, item) => total + item.numPieces, 0)
        }));
      setItemType(itemTypes);
      setChartData(counts);
      console.log(chartData)
    };     
  }, [response]);

  useEffect(() => {
    setUrl('https://marjinal-laundry.herokuapp.com/api/statistic/unit?filter='+dateRange)
  },[dateRange]);

  return (
    <div>
    {(chartData.length !== 0)?
      
      <Chart
          data={chartData}
      >
          <PieSeries
          valueField="count"
          argumentField="type"
          />
          <Legend />
          <Title
          text="Jenis pakaian satuan dicuci"
          />
      </Chart>
       :
       <h3>Tidak ada laundry satuan untuk jangka waktu {dateRange}</h3>
    }
    </div>
    
  );
}
export default ChartCard;
