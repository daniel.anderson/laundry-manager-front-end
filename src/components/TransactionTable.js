import React, {useState, useEffect} from 'react';
import {
  useTable,
  usePagination,
  useSortBy,
} from 'react-table';
import * as TransactionListConts from './TransactionListStyle';

import Link from '@material-ui/core/Link';
import axios from 'axios';

const ButtonUpdate = TransactionListConts.ButtonUpdate;
const ButtonDelete = TransactionListConts.ButtonDelete;

export default function Table({columns, data}) {

  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    
    state: {
      pageIndex,
    },
  } = useTable({
    columns,
    data,
    initialState: {
      sortBy: [
          {
              id: 'tanggalMasuk',
              desc: false
          }
      ]
    }
  },
  useSortBy,
  usePagination,
  (hooks) => {
    hooks.visibleColumns.push(columns => [
      {
        id: "action",
        Header: "Tindakan",
        Cell: ({ row }) => <CheckButton {...row} />
      },
      ...columns
      ]);
  });

  const CheckButton = (row) => {
    const [link, setLink] = useState()
    const [deleteMessage, setMessage] = useState()
    const [isError, setError] = useState()

    useEffect(() => {
      if(deleteMessage !== undefined){
        window.location.reload();
      }
    }, [deleteMessage]);

    useEffect(() => {
      const fetchData = async () => {
        try {
          const deleteMessage = await axios(link);
          setMessage(deleteMessage);
        } catch (error) {
          setError(true);
          console.log(isError)
        }
      };

      if(link !== undefined){
        fetchData()
      }
    }, [link]);

    const handleClickDelete = async () => {
      console.log(row.original)
      const id = row.original.id
      if(row.original.weight !== undefined){
        setLink('https://marjinal-laundry.herokuapp.com/api/laundry/kilogram/delete/' + id)
      }
      else{
        setLink('https://marjinal-laundry.herokuapp.com/api/laundry/unit/delete/' + id)
      }
    };
    return (
      <>
        <ButtonDelete
          onClick={handleClickDelete}
        >
          Hapus
        </ButtonDelete>
        <Link href="/updateTransaction">
          <ButtonUpdate value={row.original} >
            Memperbarui
          </ButtonUpdate>
        </Link>
      </>
    );
  };
  
  // Render the UI for your table
  return (
    <>
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
                <th 
                {...column.getHeaderProps(column.getSortByToggleProps())}
                >
                {column.render('Header')}
                <span>
                  {column.isSorted
                    ? column.isSortedDesc
                      ? ' 🔽'
                      : ' 🔼'
                    : ''}
                </span>
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {page.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </table>

    <div className="pagination">
    <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
      {'<<'}
    </button>{' '}
    <button onClick={() => previousPage()} disabled={!canPreviousPage}>
      {'<'}
    </button>{' '}
    <button onClick={() => nextPage()} disabled={!canNextPage}>
      {'>'}
    </button>{' '}
    <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
      {'>>'}
    </button>{' '}
    <span>
      Page{' '}
      <strong>
        {pageIndex + 1} of {pageOptions.length}
      </strong>{' '}
    </span>
  </div>
  </>
  )
};