import React, {useState} from 'react';
import { Grid, Paper, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DetailCard from './DetailCard';
import ChartCard from './ChartCard';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 200,
      marginTop: theme.spacing(5),
    },
    app: {
        flexGrow: 1,
        maxWidth: '752',
    },
    root2: {
        padding: theme.spacing(3, 2),
        maxWidth: '500px',
        marginLeft: theme.spacing(4),
        minHeight: '50vh',
    },
    grid: {
        width: '100%',
        padding: theme.spacing(3),
    },
    list: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
  }));

export default function Statistic() {
    const classes = useStyles();
    const [dateRange, setDateRange] = useState('1-minggu');

    const handleDateChange = (event) => {
      setDateRange(event.target.value);
    };

    return (
        <div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="select-outlined-label">Waktu</InputLabel>
                <Select
                labelId="select-outlined-label"
                id="select-outlined"
                value={dateRange}
                onChange={handleDateChange}
                label="DateRange"
                >
                <MenuItem value="1-minggu">Satu Minggu</MenuItem>
                <MenuItem value={"1-bulan"}>Satu Bulan</MenuItem>
                <MenuItem value={"3-bulan"}>Tiga Bulan</MenuItem>
                </Select>
            </FormControl>
            <div className="statistic">
            <Container maxWidth="lg" >
                <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="flex-start"
                        spacing={5}
                        className={classes.grid}
                    >
                    <Grid item xs={12} md={6} >
                        <Paper className={classes.root2} >
                            <ChartCard dateRange={dateRange}/>
                        </Paper>
                        </Grid>

                    <Grid item xs={12} md={6} >
                        <Paper className={classes.root2} >
                            <DetailCard dateRange={dateRange}/>
                        </Paper>
                    </Grid>
                        
                    </Grid>
                </Container>
            </div>
        </div>
    )
}