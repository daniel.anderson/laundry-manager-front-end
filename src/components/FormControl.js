import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 200,
      marginTop: theme.spacing(5),
    },
    app: {
        flexGrow: 1,
        maxWidth: '752',
    },
    root2: {
        padding: theme.spacing(3, 2),
        maxWidth: '500px',
        marginLeft: theme.spacing(4),
        minHeight: '50vh',
    },
    grid: {
        width: '100%',
        padding: theme.spacing(3),
    },
    list: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
  }));

function FilterTime (){

    const classes = useStyles();
    const [dateRange, setDateRange] = React.useState('1-minggu');
  
    const handleDateChange = (event) => {
      setDateRange(event.target.value);
    };

    return (
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="select-outlined-label">Waktu</InputLabel>
          <Select
          labelId="select-outlined-label"
          id="select-outlined"
          value={dateRange}
          onChange={handleDateChange}
          label="DateRange"
          >
          <MenuItem value="1-minggu">
              <em>Satu Minggu</em>
          </MenuItem>
          <MenuItem value={"1-bulan"}>Satu Bulan</MenuItem>
          <MenuItem value={"3-bulan"}>Tiga Bulan</MenuItem>
          </Select>
      </FormControl>
    )
}

export default FilterTime;