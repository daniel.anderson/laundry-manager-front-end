import React, {useState, useEffect} from 'react';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import axios from 'axios';

const DetailCard = ({ dateRange }) => {
    const [type, setType] = useState('kilogram');
    const [detail, setDetail] = useState([]);
    const [url, setUrl] = useState(
        'https://marjinal-laundry.herokuapp.com/api/statistic/'+type+'?filter='+dateRange
    )
    useEffect(() => {
        const fetchDetail = async () => {
            try{
                const response = await axios(url);
                const dataDetail = response.data;
                setDetail([dataDetail]);
            } catch (error) {
                console.log(error);
            }
        };
        fetchDetail();
    }, [url]);

    useEffect(() => {
        setUrl('https://marjinal-laundry.herokuapp.com/api/statistic/'+type+'?filter='+dateRange)
    },[dateRange])
    
    const handleType = (event, newType) => {
        if (newType !== null) {
            setType(newType);
            setUrl('https://marjinal-laundry.herokuapp.com/api/statistic/'+newType+'?filter='+dateRange)
        }
    };

    return(
        <div>
            <ToggleButtonGroup
                value={type}
                exclusive
                onChange={handleType}
                aria-label="type laundry"
                >
                <ToggleButton value="kilogram" aria-label="kilogram">
                    <p>Kiloan</p>
                </ToggleButton>
                <ToggleButton value="unit" aria-label="unit">
                    <p>Satuan</p>
                </ToggleButton>
            </ToggleButtonGroup>
            {(detail[0]!== undefined) ?
            <div>
                <h3>
                Rata-rata : {(detail[0].mean !== null)?detail[0].mean:0} {(type === 'kilogram')?'kg':'unit'}
                <br />
                Jumlah Transaksi Laundry : {detail[0].totalLaundry}
                <br />
                Uang yang digunakan : Rp.{(detail[0].totalSpend !== null)?detail[0].totalSpend:0}
                </h3>
            </div>
            : <h3>Loading...</h3>
            } 
        </div> 
    );
}

export default DetailCard;
